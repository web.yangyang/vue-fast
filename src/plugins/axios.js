/**
 * Created by yangyang on 2017/2/15.
 */
import axios from 'axios'
const instance = axios.create();

instance.interceptors.request.use(config => {
  //console.info(config)
  const token = sessionStorage.getItem("token");
  if (token != null) {
    config.headers["token"] = token;
  }
  return config;
});

instance.interceptors.response.use(response => {
  return response.data
}, err => {
  return err;
});

function plugin(Vue) {
  if (plugin.installed) {
    return;
  }
  Vue.http = instance;
}

if (typeof  window !== "undefined" && window.Vue) {
  window.Vue.use(plugin);
}

export default plugin;
