/**
 * 自定义权限指令，用于判断当前用户是否拥有某个权限
 * Created by yangyang on 2017/2/20.
 */


const auth = {
  inserted(el, binding){
    const data = this.$store.state.authorityData;
    if (!data[binding.value]) {
      const _parentElement = el.parentNode;
      if (_parentElement) {
        _parentElement.removeChild(el);
      }
    }
  }
}

export const AUTH_DIRECTIVE = auth;
