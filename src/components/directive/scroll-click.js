/**
 * 添加当前元素出现滚动条时，滚动时点击一下
 * Created by yangyang on 2017/2/22.
 */
const scrollClick = {
  inserted(el){
    $(el).scroll(function () {

    });
  }
}

export const SCROLL_CLICK_DIRECTIVE = scrollClick;
