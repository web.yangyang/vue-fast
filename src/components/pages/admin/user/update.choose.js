/**
 * 提交修改页面显示部门
 * Created by yangyang on 2017/2/22.
 */

const update = function (self, pId) {
  self.$store.commit('userDepartmentChooseStateUpdate');
  self.$store.commit('cacheDataUpdate', {
    name: 'sysUserDepartmentPid',
    value: pId
  });
}

export const updateUserChoose = update;
