/**
 * 提交修改页面显示栏目
 * Created by yangyang on 2017/2/22.
 */

const update = function (self, treeNode) {
  self.$store.commit('columnEditStateUpdate');
  self.$store.commit('cacheDataUpdate', {
    name: 'sysColumn',
    value: treeNode
  });
}

export const updateColumnChoose = update;
