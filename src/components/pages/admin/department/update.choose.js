/**
 * 提交修改页面显示部门
 * Created by yangyang on 2017/2/22.
 */

const update = function (self, treeNode) {
  self.$store.commit('departmentEditStateUpdate');
  self.$store.commit('cacheDataUpdate', {
    name: 'sysDepartment',
    value: treeNode
  });
}

export const updateDepartmentChoose = update;
