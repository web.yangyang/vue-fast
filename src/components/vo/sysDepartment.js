/**
 * Created by yangyang on 2017/2/23.
 */
export const departmentVo = function () {

  return new Object({
    id: "",
    department: {
      id: "",
      name: ""
    },
    name: "",
    fullName: "",
    codes: "",
    duty:"",
    leveler: 0,
    sorter: 0,
    formation: 1,
    realFormation: 1,
    lowFormation: 1,
    overFormation: 1,
    formationLv: 1,
    guardNo: 1,
    unGuardNo: 0,
    state: true,
    creater: "",
    creatTime: "",
    memo: "",
    reservation: ""
  });

}
