/**
 * Created by yangyang on 2017/2/22.
 */
export const columnVo = function () {
  return new Object({
    id: "",
    column: {
      id: "",
      name: ""
    },
    name: "",
    types: 0,
    leveler: "",
    backName: "",
    backUrl: "",
    backSorter: "",
    backHidden: true,
    backTarget: 0,
    beforeName: "",
    beforeUrl: "",
    beforeSorter: "",
    unable: true,
    beforeHidden: true,
    beforeTarget: 0,
    state: true,
    ifShortcut: true,
    icoSmall: "",
    icoBig: "",
    creater: "",
    creatTime: "",
    memo: "",
    reservation: ""
  });
};
