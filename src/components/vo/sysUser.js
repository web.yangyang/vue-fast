/**
 * Created by yangyang on 2017/2/24.
 */

export const userVo = function () {
  return new Object({
    id: "",
    user: {
      id: null
    },
    department: {
      id: ""
    },
    loginName: "",
    password: "",
    safeLeveler: "",
    headImg: "",
    name: "",
    nickName: "",
    types: null,
    state: true,
    OAccountBooksCode: "",
    hsn: "",
    hsy: "",
    creater: "",
    creatTime: "",
    memo: "",
    reservation: ""
  });
}
