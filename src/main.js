// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import 'element-ui/lib/theme-default/index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'assets/css/app.css'
import App from './App'
import {routes} from 'router/routes'
import {vuex} from 'vuex_/vuex'
import axios from 'plugins/axios'
import httpClient from 'plugins/httpClient'
import {SERVICE_URLS} from 'config/service.url'
import $ from 'jquery'

Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(axios)
Vue.use(httpClient)
const router = new VueRouter({
  mode: 'history',
  routes: routes
})

const store = new Vuex.Store(vuex)

Vue.directive("auth", {
  bind(el, binding){
    const data = store.state.authority;
    if (!data[binding.value]) {
      const _parentElement = el.parentNode;
      if (_parentElement) {
        _parentElement.removeChild(el);
      }
    }
  }
});

Vue.directive("scrollclick", {
  inserted(el){
    $(el).scroll(function () {
      $(this).children().click();
    });
  }
});
router.beforeEach((to, from, next) => {
  //check user nav 信息加载完成
  if (!(to.fullPath === '/login')) {
    if (!store.state['basicDataLoading']) {
      //加载各种信息
      const startTime = new Date().getTime()
      Promise.all([Vue.http.get(SERVICE_URLS.auth.currentUser.path)
        , Vue.http.get(SERVICE_URLS.column.load.path)
        , Vue.http.get(SERVICE_URLS.auth.get.path)])
        .then(res => {
          let isSuccess = true;
          if (res[0].code === 0) {
            store.commit('currentUserUpdate', res[0].body);
          } else {
            console.log("user load error msg:", res[0].message)
            isSuccess = false;
          }
          if (res[1].code === 0) {
            store.commit('cacheDataUpdate', {
              name: "columnItems",
              value: res[1].body
            });
          } else {
            console.log("column load error msg:", res[1].message)
            isSuccess = false;
          }
          if (res[2].code === 0) {
            store.commit('cacheDataUpdate', {
              name: "authority",
              value: res[2].body
            });
          } else {
            console.log("auth load error msg:", res[2].message)
            isSuccess = false;
          }
          if (isSuccess) {
            store.commit('cacheDataUpdate', {
              name: "basicDataLoading",
              value: true
            });
          }
          const endTime = new Date().getTime();
          let time = endTime - startTime;
          time = time > 1000 ? time : 1000
          setTimeout(function () {
            next();
          }, time);
        }).catch(err => {
        Message.error(err);
        next();
      })
    } else {
      next();
    }
  } else {
    next();
  }
})

router.afterEach(route => {
  store.commit('navDefaultActiveChange', route.fullPath);
})

/**
 $.fn.scrollUnique = function () {
  return $(this).each(function () {
    var eventType = 'mousewheel';
    // 火狐是DOMMouseScroll事件
    if (document.mozHidden !== undefined) {
      eventType = 'DOMMouseScroll';
    }
    $(this).on(eventType, function (event) {
      // 一些数据
      var scrollTop = this.scrollTop,
        scrollHeight = this.scrollHeight,
        height = this.clientHeight;

      var delta = (event.originalEvent.wheelDelta) ? event.originalEvent.wheelDelta : -(event.originalEvent.detail || 0);

      if ((delta > 0 && scrollTop <= delta) || (delta < 0 && scrollHeight - height - scrollTop <= -1 * delta)) {
        // IE浏览器下滚动会跨越边界直接影响父级滚动，因此，临界时候手动边界滚动定位
        this.scrollTop = delta > 0 ? 0 : scrollHeight;
        // 向上滚 || 向下滚
        event.preventDefault();
      }
    });
  });
};
 **/
/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  render: h => h(App),
  template: '<App/>',
  store: store,
  components: {App},
  router: router,
});

