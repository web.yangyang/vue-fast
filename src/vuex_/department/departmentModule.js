/**
 * Created by yangyang on 2017/2/22.
 */

const departmentModule = {
  state: {
    addState: 0,
    editState: 0,
    addDialogRef: null
  },
  mutations: {
    departmentAddStateUpdate(state){
      state.addState++;
    },
    departmentEditStateUpdate(state){
      state.editState++;
    },
    departmentAddDialogUpdate(state, value){
      state.addDialogRef = value;
    }
  }
};

export const DEPARTMENT_MODULE = departmentModule;
