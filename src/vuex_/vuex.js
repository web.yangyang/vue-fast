/**
 * Created by yangyang on 2017/2/15.
 */
export const vuex = {
  state: {
    navDefaultActive: "",
    loadingBarActive: false,
    currentUser: null,
    authority: {},
    bodyHeight: document.getElementsByTagName('body')[0].offsetHeight - 50,
  },
  mutations: {
    navDefaultActiveChange: function (state, path) {
      state.navDefaultActive = path;
    },
    currentUserUpdate(state, user){
      state.currentUser = user;
    },
    bodyHeightUpdate(state){
      state.bodyHeight = document.getElementsByTagName('body')[0].offsetHeight - 50
    },
    authorityDataUpdate(state, value){
      state.authority = value
    },
    cacheDataUpdate(state, cache){
      state[cache.name] = cache.value;
    },
  }
}
