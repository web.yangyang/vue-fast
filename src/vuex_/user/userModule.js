/**
 * Created by yangyang on 2017/2/24.
 */
const userModule = {
  state: {
    addState: 0,
    editState: 0,
    chooseState: 0,
    addDialogRef: null,
    editDialogRef: null
  },
  mutations: {
    userAddStateUpdate(state){
      state.addState++;
    },
    userEditStateUpdate(state){
      state.editState++;
    },
    userDepartmentChooseStateUpdate(state){
      state.chooseState++;
    },
    userDepartmentChooseStateReset(state){
      state.chooseState = 0;
    },
    userAddDialogUpdate(state, value){
      state.addDialogRef = value;
    },
    userEditDialogUpdate(state, value){
      state.editDialogRef = value;
    }
  }
};

export const USER_MODULE = userModule;
