/**
 * Created by yangyang on 2017/2/22.
 */

const columnModule = {
  state: {
    addState: 0,
    editState: 0,
    addDialogRef: null
  },
  mutations: {
    columnAddStateUpdate(state){
      state.addState++;
    },
    columnEditStateUpdate(state){
      state.editState++;
    },
    columnAddDialogUpdate(state, value){
      state.addDialogRef = value;
    }
  }
};

export const COLUMN_MODULE = columnModule;
