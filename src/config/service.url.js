/**
 * Created by yangyang on 2017/2/15.
 */
import {ENV} from '../config/environment'

let baseUrl = "http://127.0.0.1:9999";
const devUrl = "http://127.0.0.1:9999";
const productUrl = "http://127.0.0.1:9999";

const apiVersion = "/api/v1";

if (ENV == "dev") {
  baseUrl = devUrl;
} else if (ENV == "product") {
  baseUrl = productUrl;
}


export const SERVICE_URLS = {
  auth: {
    login: {
      path: baseUrl + apiVersion + "/auth/login",
      method: "POST",
    },
    currentUser: {
      path: baseUrl + apiVersion + "/auth/user",
      method: "GET"
    },
    get: {
      path: baseUrl + apiVersion + "/auth/get",
      method: "GET"
    }
  },
  column: {
    load: {
      path: baseUrl + apiVersion + "/system/column/load",
      method: "GET"
    },
    tree: {
      path: baseUrl + apiVersion + '/system/column/tree',
      method: "GET",
      params: {
        beginNode: {
          type: "String"
        }
      }
    },
    select: {
      path: baseUrl + apiVersion + '/system/column/select',
      method: "GET",
      params: {
        id: {
          type: "String"
        }
      }
    },
    update: {
      path: baseUrl + apiVersion + '/system/column/update/{id}',
      params: {
        id: {
          type: "String"
        }
      },
      method: "PUT"
    },
    create: {
      path: baseUrl + apiVersion + '/system/column/create',
      method: "POST"
    },
    delete: {
      path: baseUrl + apiVersion + '/system/column/del/{id}',
      params: {
        id: {
          type: "String"
        }
      },
      method: "DELETE"
    }
  },
  department: {
    tree: {
      path: baseUrl + apiVersion + "/system/department/tree",
      method: "GET",
      params: {
        checkedListStr: "String"
      }
    },
    delete: {
      path: baseUrl + apiVersion + '/system/department/del/{id}',
      params: {
        id: {
          type: "String"
        }
      },
      method: "DELETE"
    },
    select: {
      path: baseUrl + apiVersion + '/system/department/select',
      method: "GET",
      params: {
        id: {
          type: "String"
        }
      }
    },
    update: {
      path: baseUrl + apiVersion + '/system/department/update/{id}',
      method: "PUT",
      params: {
        id: {
          type: "String"
        }
      }
    },
    create: {
      path: baseUrl + apiVersion + '/system/department/create',
      method: "POST"
    }
  },
  user: {
    userInfo: {
      path: baseUrl + apiVersion + "/system/user/userInfo",
      method: "PUT"
    },
    password: {
      path: baseUrl + apiVersion + "/system/user/password",
      method: "PUT"
    },
    checkPassword: {
      path: baseUrl + apiVersion + "/system/user/checkPassword",
      method: "POST"
    },
    create: {
      path: baseUrl + apiVersion + '/system/user/create',
      method: "POST"
    },
    search: {
      path: baseUrl + apiVersion + '/system/user/search',
      method: "GET",
      params: {
        department: {
          id: "String"  //required
        },
        loginName: "String", //not support like
        name: "String",
        state: "Boolean"
      }
    },
    delete: {
      path: baseUrl + apiVersion + '/system/user/del/{id}',
      method: 'DELETE',
      params: {
        id: 'String'
      }
    },
    deleteMore: {
      path: baseUrl + apiVersion + '/system/user/delMore',
      method: 'DELETE',
      params: {
        idList: "List"
      }
    },
    view: {
      path: baseUrl + apiVersion + '/system/user/view/{id}',
      method: 'GET',
      params: {
        id: "String"
      }
    },
    update: {
      path: baseUrl + apiVersion + '/system/user/update/{id}',
      method: 'PUT',
      params: {
        id: "String"
      }
    },
    reset: {
      path: baseUrl + apiVersion + '/system/user/reset/{id}',
      method: "PUT",
      params: {
        id: "String"
      }
    }
  },
  role: {
    search: {
      path: baseUrl + apiVersion + '/system/role/search',
      method: "GET"
    },
    create: {
      path: baseUrl + apiVersion + '/system/role/create',
      method: "POST"
    },
    update: {
      path: baseUrl + apiVersion + '/system/role/update/{id}',
      method: "PUT",
      params: {
        id: "String"
      }
    },
    delete: {
      path: baseUrl + apiVersion + '/system/role/del/{id}',
      method: "DELETE",
      params: {
        id: "String"
      }
    },
    deleteMore: {
      path: baseUrl + apiVersion + '/system/role/delMore',
      method: 'DELETE',
      params: {
        idList: "List"
      }
    },
    view: {
      path: baseUrl + apiVersion + '/system/role/view/{id}',
      method: 'GET',
      params: {
        id: "String"
      }
    },
  },
  params: {
    select: {
      path: baseUrl + apiVersion + "/system/params/select/{id}",
      params: {
        id: "String",
      },
      method: "GET"
    }
  }
};
