/**
 * Created by yangyang on 2017/2/9.
 */

import Login from '../components/login/login.vue'
import Index from '../components/pages/index.vue'

const UserBasicInfo = resolve => require(['components/pages/admin/user/basicInfo.vue'], resolve)
const UserPassWord = resolve => require(['components/pages/admin/user/password.vue'], resolve)

const SysColumnCenter = resolve => require(['components/pages/admin/column/center.vue'], resolve)
const SysDepartmentCenter = resolve => require(['components/pages/admin/department/center.vue'], resolve)
const SysUserCenter = resolve => require(['components/pages/admin/user/center.vue'], resolve)
const SysUserTab = resolve => require(['components/pages/admin/user/tab.vue'], resolve)
const SysRoleTab = resolve => require(['components/pages/admin/role/tab.vue'], resolve)

let routeUrl = {
  index: {
    absolutePath: "/index", title: "登陆后的主体"
  },
  user: {
    baseInfo: {
      absolutePath: "/index/user/basicInfo", title: "基本信息"
    },
    tab: {
      absolutePath: "/index/user/center/tab/", title: "用户列表"
    },
    center: {
      absolutePath: "/index/user/center", title: "用户中心"
    }
  },
  auth: {
    login: {
      absolutePath: "/login", title: "登陆"
    }
  }
}

export const routes = [
  {
    path: "/login", component: Login
  },
  {
    path: "/index", component: Index,
    children: [
      {
        path: 'user/basicInfo', component: UserBasicInfo
      },
      {
        path: 'user/password', component: UserPassWord
      },
      {
        path: 'column/center', component: SysColumnCenter
      },
      {
        path: 'department/center', component: SysDepartmentCenter
      },
      {
        path: 'user/center', component: SysUserCenter,
        children: [
          {
            path: "tab/:pId", component: SysUserTab
          }
        ]
      },
      {
        path: "role", component: SysRoleTab
      }

    ]
  }
];

export const ROUTE_URL = routeUrl;


